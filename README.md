# Twitter followers

This course is being powered by [Skillify](https://www.skillify.tech)

[<img src="https://d31ezp3r8jwmks.cloudfront.net/rt2n3ikxlkwsqrwyqzmo78mmm743" alt="Twitter followers" width="462" />](https://www.skillify.tech/construye-un-sistema-de-seguidores)

> Backend

Bootstrapped with:

- Node.js lts/fermium
- Express
- MongoDB

> Frontend

Bootstrapped with:

- Create React App
- Styled components
- React router

> Cloud

- Heroku
- Mongo Atlas
- Netlify

## Learn More

You can learn more in [Skillify courses](https://www.skillify.tech/courses).
